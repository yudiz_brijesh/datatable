import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DtTableService } from '../shared/services/dt-table.service';

@Component({
  selector: 'app-dt-table',
  templateUrl: './dt-table.component.html',
  styleUrls: ['./dt-table.component.css']
})
export class DtTableComponent implements OnInit {
  public loadingIndicator = true;
  @Input() columns;
  @Input() rows;
  @Output() sendPageNumber = new EventEmitter();
  constructor() { }
  ngOnInit(): void {
    if (this.rows) {
      this.loadingIndicator = false;
    }
  }
  setPage(event) {
    this.sendPageNumber.emit(event);
  }

}
