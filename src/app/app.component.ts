import { Component, OnInit } from '@angular/core';
import { Data } from './shared/data.class';
import { DtTableService } from './shared/services/dt-table.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private service: DtTableService) { }
  title = 'datatable';
  public pageNumber = 0;
  public limit = 10;
  rows: Data;
  columns = [
    'firstName',
    'lastName',
    'email',
  ];
  getPageNumber(event) {
    this.pageNumber = event.offset;
    this.limit = event.limit;
    this.service.getData(this.limit, this.pageNumber);
  }
  ngOnInit() {
    this.service.getData(this.limit, this.pageNumber);
    this.getData();
  }
  getData() {
    this.service.returnData().subscribe((data: Data) => {
      this.rows = data;
    });
  }
}
