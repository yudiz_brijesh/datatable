export class Data {
  data: [{
    id: string,
    firstName: string,
    picture: string,
    lastName: string,
    email: string ,
    title: string
  }];
    total: number;
    page: number;
    limit: number;
    offset: number;
}
