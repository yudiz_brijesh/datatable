import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Data } from '../data.class';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DtTableService {
  public rows: BehaviorSubject<any> = new BehaviorSubject(Data);
  constructor(private http: HttpClient) {}
  getData(limit, pageNo) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'app-id': '5f6c37cde4ecc415ab0b1b91'
      })
    };
    this.http.get(environment.apiUrl + `data/api/user?limit=${limit}&page=${pageNo}`, httpOptions).subscribe((data: []) => {
      this.rows.next(data);
    });
  }
  returnData() {
    return this.rows.asObservable();
  }
}
